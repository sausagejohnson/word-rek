##
## Auto Generated makefile by CodeLite IDE
## any manual changes will be erased      
##
## Debug_x64
ProjectName            :=word-rek
ConfigurationName      :=Debug_x64
WorkspacePath          :=C:/Work/Dev/orx-projects/word-rek/build/windows/codelite
ProjectPath            :=C:/Work/Dev/orx-projects/word-rek/build/windows/codelite
IntermediateDirectory  :=$(ConfigurationName)
OutDir                 := $(IntermediateDirectory)
CurrentFileName        :=
CurrentFilePath        :=
CurrentFileFullPath    :=
User                   :=sausage
Date                   :=22/07/2021
CodeLitePath           :="C:/Program Files/CodeLite"
LinkerName             :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe
SharedObjectLinkerName :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe -shared -fPIC
ObjectSuffix           :=.o
DependSuffix           :=.o.d
PreprocessSuffix       :=.i
DebugSwitch            :=-g 
IncludeSwitch          :=-I
LibrarySwitch          :=-l
OutputSwitch           :=-o 
LibraryPathSwitch      :=-L
PreprocessorSwitch     :=-D
SourceSwitch           :=-c 
OutputFile             :=../../../bin/word-rekd.exe
Preprocessors          :=$(PreprocessorSwitch)__orxDEBUG__ 
ObjectSwitch           :=-o 
ArchiveOutputSwitch    := 
PreprocessOnlySwitch   :=-E
ObjectsFileList        :="word-rek.txt"
PCHCompileFlags        :=
MakeDirCommand         :=makedir
RcCmpOptions           := 
RcCompilerName         :=C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/windres.exe
LinkOptions            :=  -static-libgcc -static-libstdc++ -m64 -L/usr/lib64
IncludePath            :=  $(IncludeSwitch). $(IncludeSwitch)$(ORX)/include $(IncludeSwitch)../../../include/Scroll $(IncludeSwitch)../../../include 
IncludePCH             := 
RcIncludePath          := 
Libs                   := $(LibrarySwitch)orxd 
ArLibs                 :=  "orxd" 
LibPath                := $(LibraryPathSwitch). $(LibraryPathSwitch)$(ORX)/lib/dynamic $(LibraryPathSwitch). 

##
## Common variables
## AR, CXX, CC, AS, CXXFLAGS and CFLAGS can be overriden using an environment variables
##
AR       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/ar.exe rcu
CXX      := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/g++.exe
CC       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/gcc.exe
CXXFLAGS :=  -ffast-math -g -msse2 -m64 -fno-exceptions $(Preprocessors)
CFLAGS   :=  -ffast-math -g -msse2 -m64 $(Preprocessors)
ASFLAGS  := 
AS       := C:/MinGW-w64/x86_64-8.1.0-win32-seh/mingw64/bin/as.exe


##
## User defined environment variables
##
CodeLiteDir:=C:\Program Files (x86)\CodeLite
CC:=x86_64-w64-mingw32-gcc
CXX:=x86_64-w64-mingw32-g++
AR:=x86_64-w64-mingw32-gcc-ar
Objects0=$(IntermediateDirectory)/up_up_up_src_Word.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_word-rek.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_TitleScreen.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Scope.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_Cannon.cpp$(ObjectSuffix) $(IntermediateDirectory)/up_up_up_src_GameOver.cpp$(ObjectSuffix) 



Objects=$(Objects0) 

##
## Main Build Targets 
##
.PHONY: all clean PreBuild PrePreBuild PostBuild MakeIntermediateDirs
all: $(OutputFile)

$(OutputFile): $(IntermediateDirectory)/.d $(Objects) 
	@$(MakeDirCommand) $(@D)
	@echo "" > $(IntermediateDirectory)/.d
	@echo $(Objects0)  > $(ObjectsFileList)
	$(LinkerName) $(OutputSwitch)$(OutputFile) @$(ObjectsFileList) $(LibPath) $(Libs) $(LinkOptions)

PostBuild:
	@echo Executing Post Build commands ...
	cmd /c copy /Y C:\Work\Dev\orx\code\lib\dynamic\orx*.dll ..\..\..\bin
	@echo Done

MakeIntermediateDirs:
	@$(MakeDirCommand) "$(ConfigurationName)"


$(IntermediateDirectory)/.d:
	@$(MakeDirCommand) "$(ConfigurationName)"

PreBuild:


##
## Objects
##
$(IntermediateDirectory)/up_up_up_src_Word.cpp$(ObjectSuffix): ../../../src/Word.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Word.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Word.cpp$(DependSuffix) -MM ../../../src/Word.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/word-rek/src/Word.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Word.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Word.cpp$(PreprocessSuffix): ../../../src/Word.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Word.cpp$(PreprocessSuffix) ../../../src/Word.cpp

$(IntermediateDirectory)/up_up_up_src_word-rek.cpp$(ObjectSuffix): ../../../src/word-rek.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_word-rek.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_word-rek.cpp$(DependSuffix) -MM ../../../src/word-rek.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/word-rek/src/word-rek.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_word-rek.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_word-rek.cpp$(PreprocessSuffix): ../../../src/word-rek.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_word-rek.cpp$(PreprocessSuffix) ../../../src/word-rek.cpp

$(IntermediateDirectory)/up_up_up_src_TitleScreen.cpp$(ObjectSuffix): ../../../src/TitleScreen.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_TitleScreen.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_TitleScreen.cpp$(DependSuffix) -MM ../../../src/TitleScreen.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/word-rek/src/TitleScreen.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_TitleScreen.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_TitleScreen.cpp$(PreprocessSuffix): ../../../src/TitleScreen.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_TitleScreen.cpp$(PreprocessSuffix) ../../../src/TitleScreen.cpp

$(IntermediateDirectory)/up_up_up_src_Scope.cpp$(ObjectSuffix): ../../../src/Scope.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Scope.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Scope.cpp$(DependSuffix) -MM ../../../src/Scope.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/word-rek/src/Scope.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Scope.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Scope.cpp$(PreprocessSuffix): ../../../src/Scope.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Scope.cpp$(PreprocessSuffix) ../../../src/Scope.cpp

$(IntermediateDirectory)/up_up_up_src_Cannon.cpp$(ObjectSuffix): ../../../src/Cannon.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_Cannon.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_Cannon.cpp$(DependSuffix) -MM ../../../src/Cannon.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/word-rek/src/Cannon.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_Cannon.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_Cannon.cpp$(PreprocessSuffix): ../../../src/Cannon.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_Cannon.cpp$(PreprocessSuffix) ../../../src/Cannon.cpp

$(IntermediateDirectory)/up_up_up_src_GameOver.cpp$(ObjectSuffix): ../../../src/GameOver.cpp
	@$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) -MG -MP -MT$(IntermediateDirectory)/up_up_up_src_GameOver.cpp$(ObjectSuffix) -MF$(IntermediateDirectory)/up_up_up_src_GameOver.cpp$(DependSuffix) -MM ../../../src/GameOver.cpp
	$(CXX) $(IncludePCH) $(SourceSwitch) "C:/Work/Dev/orx-projects/word-rek/src/GameOver.cpp" $(CXXFLAGS) $(ObjectSwitch)$(IntermediateDirectory)/up_up_up_src_GameOver.cpp$(ObjectSuffix) $(IncludePath)
$(IntermediateDirectory)/up_up_up_src_GameOver.cpp$(PreprocessSuffix): ../../../src/GameOver.cpp
	$(CXX) $(CXXFLAGS) $(IncludePCH) $(IncludePath) $(PreprocessOnlySwitch) $(OutputSwitch) $(IntermediateDirectory)/up_up_up_src_GameOver.cpp$(PreprocessSuffix) ../../../src/GameOver.cpp


-include $(IntermediateDirectory)/*$(DependSuffix)
##
## Clean
##
clean:
	$(RM) -r $(ConfigurationName)/



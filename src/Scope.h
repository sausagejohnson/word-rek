/**
 * @file Scope.h
 * @date 25-Jun-2021
 */

#ifndef __SCOPE_H__
#define __SCOPE_H__

#include "word-rek.h"

/** Object Class
 */
class Scope : public ScrollObject
{
public:

protected:

                void            OnCreate();
                void            OnDelete();
                void            Update(const orxCLOCK_INFO &_rstInfo);
				orxBOOL 		OnCollide(ScrollObject *_poCollider,
											const orxSTRING _zPartName,
											const orxSTRING _zColliderPartName,
											const orxVECTOR &_rvPosition,
											const orxVECTOR &_rvNormal);

private:
	//int 
};

#endif // __SCOPE_H__

/**
 * @file Word.cpp
 * @date 25-Jun-2021
 */

#include "Word.h"

void Word::OnCreate()
{
    orxConfig_SetBool("IsObject", orxTRUE);
	
	guid = this->GetGUID();
	word_rek::GetInstance().AddWordGuidToCache(guid);
	//orxLOG("cache size: %d", word_rek::GetInstance().GetCountFromCache());
 	
	//When created, go through all siblings looking for the current word.
	//If none are, then make this one it.
	//After that, make 25% of the words, the current word.
	//Then play the sound for the word.
	const orxSTRING currentTargetWord = word_rek::GetInstance().GetTargetWord();
	
	int totalWordsInPlay = word_rek::GetInstance().GetCountFromCache();
	int totalWordsEqualToCurrentWord = 0;

	int i;
	for (i=0; i<totalWordsInPlay; i++){
		orxU64 iGuid = word_rek::GetInstance().GetGuidAtIndex(i);
		ScrollObject *wordScrollObject = word_rek::GetInstance().GetObject(iGuid);
		if (wordScrollObject){
			orxOBJECT *wordObject = wordScrollObject->GetOrxObject();
			const orxSTRING wordText = orxObject_GetTextString(wordObject);
			if (orxString_Compare(wordText, currentTargetWord) == 0){
				totalWordsEqualToCurrentWord++;
			}
		}
	}

	int percentageOfWordsAreCurrent = ((orxFLOAT)totalWordsEqualToCurrentWord/(orxFLOAT)totalWordsInPlay)*100;
	
	//Ensure the first word must be on subject
	if (percentageOfWordsAreCurrent == 0){
		orxOBJECT *thisWordObject = this->GetOrxObject();
		orxObject_SetTextString(thisWordObject, currentTargetWord);
		AutoResizeBody();
		orxLOG("word will now be *%s*.", currentTargetWord);
	} else {
		if (percentageOfWordsAreCurrent < 50){// 25) {
			//make it a 1 in 2 chance of being on subject
			int oneOrTwo = orxMath_GetRandomU32(1, 2);
			if (oneOrTwo == 1){
				orxOBJECT *thisObject = this->GetOrxObject();
				orxObject_SetTextString(thisObject, currentTargetWord);
				AutoResizeBody();
			}
			//Otherwise stick to whatever the random is in the config.
		}
	}
	
	//orxOBJECT *thisObject = this->GetOrxObject();
	//orxBody_CreateFromConfig((orxSTRUCTURE*)thisObject, "WordBody");
	
	word_rek::GetInstance().TryFlashOnWord(this);
	this->AddFX("FadeIn", orxFALSE);
	
}



void Word::OnDelete()
{
	word_rek::GetInstance().RemoveWordGuidFromCache(guid);
	
	//orxLOG("remove cache size: %d", word_rek::GetInstance().GetCountFromCache());
	
	orxCHAR stringGUID[64] = {};
	orxString_NPrint(stringGUID, sizeof(stringGUID) - 1, "%lld", guid);
	
	const orxSTRING list[] = { stringGUID };
	

}

void Word::AutoResizeBody(){
	orxOBJECT *thisObject = this->GetOrxObject();
	
	//orxANIMPOINTER *animPointer = orxOBJECT_GET_STRUCTURE(thisObject, orxSTRUCTURE_ID_GRAPHIC);
	orxGRAPHIC *currentFrameGraphic = orxOBJECT_GET_STRUCTURE(thisObject, GRAPHIC);
	orxVECTOR currentFrameSize;
	orxGraphic_GetSize(currentFrameGraphic, &currentFrameSize);
	
	orxVECTOR currentFramePivot;
	orxGraphic_GetPivot (currentFrameGraphic, &currentFramePivot); 
	
	orxSTRUCTURE *bodyStructure = _orxObject_GetStructure(thisObject, orxSTRUCTURE_ID_BODY);

	orxBODY *body = (orxBODY*)bodyStructure;
	orxBODY_PART *part = orxBody_GetNextPart(body, orxNULL);
	
	orxBODY_PART_DEF partDef;
	orxMemory_Copy(&partDef, orxBody_GetPartDef(part), sizeof(orxBODY_PART_DEF));
		
	orxVECTOR vTL;
	orxVECTOR vBR;
	vTL.fX = - currentFramePivot.fX; 
	vTL.fY = - currentFramePivot.fY;
	vBR.fX = currentFrameSize.fX - currentFramePivot.fX;
	vBR.fY = currentFrameSize.fY - currentFramePivot.fY;
	
	orxAABOX box = partDef.stAABox.stBox;
	partDef.stAABox.stBox.vTL.fX = vTL.fX;
	partDef.stAABox.stBox.vTL.fY = vTL.fY;
	partDef.stAABox.stBox.vBR.fX = vBR.fX;
	partDef.stAABox.stBox.vBR.fY = vBR.fY;
	
	orxBody_RemovePart(part);
	orxBody_AddPart(body, &partDef);
}

void Word::Update(const orxCLOCK_INFO &_rstInfo)
{
	orxVECTOR position = {0, 0, 0};
	this->GetPosition(position, orxFALSE);
	if (position.fY > 700){
		this->SetLifeTime(0); //safety net
	}
	
}

orxBOOL Word::OnCollide(ScrollObject *_poCollider,
	const orxSTRING _zPartName,
	const orxSTRING _zColliderPartName,
	const orxVECTOR &_rvPosition,
	const orxVECTOR &_rvNormal)
{
	const orxSTRING colliderName = _poCollider->GetModelName();
 	const orxSTRING currentTargetWord = word_rek::GetInstance().GetTargetWord();
	
	

	if (orxString_Compare(colliderName, "Flack") == 0)
	{
		word_rek::GetInstance().testCollisions++;
		orxLOG("test collisions = %d", word_rek::GetInstance().testCollisions);
	
		orxOBJECT *thisObject = this->GetOrxObject();
		const orxSTRING thisWord = orxObject_GetTextString(thisObject);
		if (orxString_Compare(thisWord, currentTargetWord) == 0){
			_poCollider->SetLifeTime(0.0);
			
			orxVECTOR position = {0,0,0};
			this->GetPosition(position, orxFALSE);
			
			this->SetLifeTime(0.0);
			
			
			orxOBJECT *explosion = orxObject_CreateFromConfig("Explosions");
			orxObject_SetPosition(explosion, &position);

			word_rek::GetInstance().IncreaseWordSuccess();
		}
	}
 
	return orxTRUE;
}

/**
 * @file word-rek.h
 * @date 25-Jun-2021
 */

#ifndef __word_rek_H__
#define __word_rek_H__

#define __NO_SCROLLED__
#include <vector>
#include "Scroll.h"


/** Game Class
 */
class word_rek : public Scroll<word_rek>
{
public:
int testCollisions = 0;
				orxBOOL		GunTriggerWasPressed();
				void		ClearGunTrigger();
				void		SetGunTrigger();
				const orxSTRING GetTargetWord();
				
				void		AddWordGuidToCache(orxU64 guid);
				void		RemoveWordGuidFromCache(orxU64 guid);
				int			GetCountFromCache();
				orxU64		GetGuidAtIndex(int index);
				
				void 		UpdateFLashingOnAllWords();
				void		TryFlashOnWord(ScrollObject *wordObject);

				void		IncreaseWordSuccess(); //how many shot
				
				orxBOOL 	GameIsActive();
				void		ShowTitleScreen();
				void		HideTitleScreen();
				
				void EndTheGame();
				void StartNewGame();
				orxBOOL IsOkToStartNewGame();
				void AllowNewGame();
				
				ScrollObject *scene;
private:

                orxSTATUS       Bootstrap() const;

                void            Update(const orxCLOCK_INFO &_rstInfo);
				void 			WordUpdate();
				void			BoostUpLowestWords();

                orxSTATUS       Init();
                orxSTATUS       Run();
                void            Exit();
                void            BindObjects();

				ScrollObject *scope;
				ScrollObject *cannon;
				orxOBJECT *wordToTarget;
				orxOBJECT *hitsUI;
				orxOBJECT *countDownUI;
				
				ScrollObject *titleScrollObject;
				ScrollObject *gameOverScrollObject;

				ScrollObject *titleScreen;

				orxBOOL gunTriggerFlag = orxFALSE;
				int framesSinceUpdate = 0;
				const int FRAMES_PER_WORD_UPDATE = 60*16;//20; 
				
				int secondsRemaingInGame = 0;
				int ticks = 0;
				const int TICKS_PER_SECOND_MAX = 60;
				
				void PlaySound(const orxSTRING soundFromConfig);
				void Encourage();
				
				orxBOOL gameIsActive = orxFALSE;
				orxBOOL okToStartNewGame = orxTRUE;

				void DontAllowNewGame();
				
				std::vector<orxU64> wordCache;
				int countWordsSuccessful = 0;
				
				int wordsShotPerRound = 0; // 0 to 5 capped. Must always +1 to avoid div by zero. 
									// greater almost no flash. max 5 is max skill cap at 5.
									// 1.0 / 5 skill = 0.2, so vector flash is: 
									// 			1.0, 0.5 + ((1.0 - 0.2)/2), (1.0 - 0.2)  ::: 1.0, 0.9, 0.8
									// 1.0 / 1 skill = 1.0, so vector flash is:
									//			1.0, 0.5 + ((1.0 - 1.0 /2), (1.0 - 1.0)  ::: 1.0. 0.5, 0.0
				orxVECTOR flashLevel = {1.0, 0.5, 0.0};
};

#endif // __word_rek_H__

/**
 * @file Scope.h
 * @date 25-Jun-2021
 */

#ifndef __CANNON_H__
#define __CANNON_H__

#include "word-rek.h"

/** Object Class
 */
class Cannon : public ScrollObject
{
public:


protected:

                void            OnCreate();
                void            OnDelete();
                void            Update(const orxCLOCK_INFO &_rstInfo);


private:
				ScrollObject	*gunTrigger;
};

#endif // __CANNON_H__

/**
 * @file Object.cpp
 * @date 25-Jun-2021
 */

#include "Cannon.h"
#define MAXIMUM_Y_MOUSE_POS 550

void Cannon::OnCreate()
{
    orxConfig_SetBool("IsObject", orxTRUE);
	ScrollObject *gun = this->GetOwnedChild();
	gunTrigger = gun->GetOwnedChild();
	gunTrigger->Enable(orxFALSE, orxFALSE);
}

void Cannon::OnDelete()
{
}

void Cannon::Update(const orxCLOCK_INFO &_rstInfo)
{
	orxVECTOR cannonPosition = orxVECTOR_0;
	this->GetPosition(cannonPosition, orxFALSE);
	//orxObject_GetPosition(this, &arrowPosition);
	 
	orxVECTOR mousePosition = orxVECTOR_0;
	orxMouse_GetPosition(&mousePosition);
	if (mousePosition.fY > MAXIMUM_Y_MOUSE_POS) {
		return;
	}
	//orxLOG("Y: %f", mousePosition.fY);
	orxRender_GetWorldPosition(&mousePosition, orxNULL, &mousePosition);
	 
	orxVECTOR direction = orxVECTOR_0;
	orxVector_Add(&direction, &cannonPosition, &mousePosition);
	 
	orxFLOAT angle = orxMath_ATan(direction.fY, direction.fX) - (orxMATH_KF_DEG_TO_RAD * 270);
	
	this->SetRotation(angle, orxFALSE);
	
	if (gunTrigger->IsEnabled()){
		gunTrigger->Enable(orxFALSE, orxFALSE);
	}
	
	//Check for trigger press ad try and turn on spawner
	if (word_rek::GetInstance().GunTriggerWasPressed()){
		word_rek::GetInstance().ClearGunTrigger();
		ScrollObject *gun = this->GetOwnedChild();
		ScrollObject *gunTrigger = gun->GetOwnedChild();
		gunTrigger->Enable(orxTRUE, orxFALSE);
		this->AddSound("ShootSound");
	}


}

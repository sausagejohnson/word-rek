/**
 * @file Word.h
 * @date 25-Jun-2021
 */

#ifndef __WORD_H__
#define __WORD_H__

#include "word-rek.h"

/** Object Class
 */
class Word : public ScrollObject
{
public:

protected:

                void            OnCreate();
                void            OnDelete();
                void            Update(const orxCLOCK_INFO &_rstInfo);
				orxBOOL 		OnCollide(ScrollObject *_poCollider,
											const orxSTRING _zPartName,
											const orxSTRING _zColliderPartName,
											const orxVECTOR &_rvPosition,
											const orxVECTOR &_rvNormal);

private:
				int				GetIndexForWord(const orxSTRING word);
				void			PushObjectGUID();
				void			DeleteObjectGUID();
				
				void			AutoResizeBody();
				
				orxU64 guid;
};

#endif // __WORD_H__

/**
 * @file Object.cpp
 * @date 25-Jun-2021
 */

#include "Scope.h"

void Scope::OnCreate()
{
    orxConfig_SetBool("IsObject", orxTRUE);
}

void Scope::OnDelete()
{
}


void Scope::Update(const orxCLOCK_INFO &_rstInfo)
{
	orxVECTOR mousePosition = {0,0,0};
	orxMouse_GetPosition(&mousePosition);
	
	orxFLOAT zPos = mousePosition.fZ;
	orxFLOAT xPos = mousePosition.fX;
	
	if (xPos < 0) {
		xPos = 0.1;
	}
	orxFLOAT rotation = ((xPos / (orxFLOAT)1024));// - (orxFLOAT)0.5);
	
	//orxLOG("y: %f", mousePosition.fY);
	
	orxRender_GetWorldPosition(&mousePosition, orxNULL, &mousePosition);
	mousePosition.fZ = zPos;
	this->SetPosition(mousePosition);
	this->SetRotation(1.0 - (rotation - 0.5), orxFALSE);
	
	if (orxInput_HasBeenActivated("LeftClick") == orxTRUE){
		if (word_rek::GetInstance().GameIsActive()){
			word_rek::GetInstance().SetGunTrigger();
		} else {
			word_rek::GetInstance().StartNewGame();
		}
		
	}
	
}

orxBOOL Scope::OnCollide(ScrollObject *_poCollider,
	const orxSTRING _zPartName,
	const orxSTRING _zColliderPartName,
	const orxVECTOR &_rvPosition,
	const orxVECTOR &_rvNormal)
{
	const orxSTRING colliderName = _poCollider->GetModelName();
 
	if (orxString_Compare(colliderName, "Laser") == 0)
	{
		_poCollider->SetLifeTime(0.0);
		orxVECTOR position = {0,0,0};
		this->GetPosition(position, orxFALSE);
		orxOBJECT *flack = orxObject_CreateFromConfig("Flack");
		orxObject_SetPosition(flack, &position);
		
	}
 
	return orxTRUE;
}

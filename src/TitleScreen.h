
#ifndef __TITLESCREEN_H__
#define __TITLESCREEN_H__

#include "word-rek.h"

/** Object Class
 */
class TitleScreen : public ScrollObject
{
public:


protected:

                void            OnCreate();
                void            OnDelete();
                void            Update(const orxCLOCK_INFO &_rstInfo);


private:

};

#endif // __TITLESCREEN_H__

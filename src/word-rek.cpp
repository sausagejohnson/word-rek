/**
 * @file word-rek.cpp
 * @date 25-Jun-2021
 */

#define __SCROLL_IMPL__
#include "word-rek.h"
#undef __SCROLL_IMPL__

#include "Scope.h"
#include "Cannon.h"
#include "Word.h"
#include "GameOver.h"
#include "TitleScreen.h"

#ifdef __orxMSVC__

/* Requesting high performance dedicated GPU on hybrid laptops */
__declspec(dllexport) unsigned long NvOptimusEnablement        = 1;
__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;

#endif // __orxMSVC__

orxBOOL word_rek::GunTriggerWasPressed()
{
	return gunTriggerFlag;
};

void word_rek::ClearGunTrigger()
{
	gunTriggerFlag = orxFALSE;
};

void word_rek::SetGunTrigger()
{
	gunTriggerFlag = orxTRUE;
};

/** Update function, it has been registered to be called every tick of the core clock
 */
void word_rek::Update(const orxCLOCK_INFO &_rstInfo)
{
	if (orxInput_HasBeenActivated("Test"))
	{
		if (GameIsActive()){
			EndTheGame();
		}
	}
	
	if (orxInput_HasBeenActivated("NewGameKey"))
	{
			StartNewGame();
	}

	if (orxInput_HasBeenActivated("LogAll"))
	{
			orxStructure_LogAll();
	}
	
	
	
	
    if(orxInput_IsActive("Quit"))
    {
        orxEvent_SendShort(orxEVENT_TYPE_SYSTEM, orxSYSTEM_EVENT_CLOSE);
    }
	
	if (!GameIsActive()){
		return;
	}
	
	framesSinceUpdate++;
	if (framesSinceUpdate >= FRAMES_PER_WORD_UPDATE){
		framesSinceUpdate = 0;
		WordUpdate();
	}
	if (framesSinceUpdate > 0 && framesSinceUpdate % 600 == 0) {
		if (wordsShotPerRound < 1)
			wordsShotPerRound = 1;
		if (wordsShotPerRound > 4)
			wordsShotPerRound = 4;
			
		orxFLOAT skill = 1.0f / (orxFLOAT)wordsShotPerRound;
		if (skill <= 0.2)
			skill = 0.0;
		
		orxFLOAT green = 0.5f + ((1.0f - skill)/2.0f);
		orxFLOAT blue =  1.0f - skill;
		flashLevel.fY = green;
		flashLevel.fZ = blue;

		//orxLOG("skillz %f green %f blue %f", skill, green, blue);
		// 0.5 0.75, 0.5
		//doesn't always go back to heavy flashing.
		
		wordsShotPerRound = 0;
	}
	
	//tick over ticks so we can count down the game
	if (secondsRemaingInGame > 0){
		if (ticks < TICKS_PER_SECOND_MAX){
			ticks++;
		} else {
			ticks = 0;
			secondsRemaingInGame--;
			orxCHAR formattedCountDown[5] = {};
			orxString_NPrint(formattedCountDown, sizeof(formattedCountDown) - 1, "%d", secondsRemaingInGame);
			orxObject_SetTextString(countDownUI, formattedCountDown); 
		}
	} else if (secondsRemaingInGame == 0){
		EndTheGame();
	}
	

	
}

void word_rek::ShowTitleScreen(){
	titleScreen->Enable(orxTRUE);
}


void word_rek::HideTitleScreen(){
	titleScreen->Enable(orxFALSE);	
}

orxBOOL word_rek::IsOkToStartNewGame(){
	return okToStartNewGame;
}

void word_rek::DontAllowNewGame(){
	okToStartNewGame = orxFALSE;
}

void word_rek::AllowNewGame(){
	okToStartNewGame = orxTRUE;
}

void word_rek::StartNewGame(){
	if (!GameIsActive() && IsOkToStartNewGame()){
		// Create the scene
		scene = CreateObject("Scene");
		cannon = CreateObject("Cannon");
		
		wordToTarget = orxObject_CreateFromConfig("WordToTarget");
		hitsUI = orxObject_CreateFromConfig("HitsUI");
		countDownUI = orxObject_CreateFromConfig("CountDownUI");
		
		gameIsActive = orxTRUE;
		DontAllowNewGame();
		HideTitleScreen();
		
		countWordsSuccessful = 0;
		secondsRemaingInGame = 120;
		
		WordUpdate();
	}
}

void word_rek::EndTheGame(){
	if (GameIsActive()){
		gameIsActive = orxFALSE;
		scene->SetLifeTime(0.0);
		cannon->SetLifeTime(0.0);
		orxObject_SetLifeTime(wordToTarget, 0.0);
		orxObject_SetLifeTime(hitsUI, 0.0);
		orxObject_SetLifeTime(countDownUI, 0.0);
		ScrollObject *gameOver = CreateObject("GameOver");
		ScrollObject *gameOverScoreScrollObject = gameOver->GetOwnedChild();
		orxOBJECT *gameOverScoreObject = gameOverScoreScrollObject->GetOrxObject();
		
		orxCHAR formattedScoreSentence[20] = {};
		orxString_NPrint(formattedScoreSentence, sizeof(formattedScoreSentence) - 1, "your score was %d", countWordsSuccessful);
		orxObject_SetTextString(gameOverScoreObject, formattedScoreSentence);
	}
}

orxBOOL word_rek::GameIsActive(){
	return gameIsActive;
}

void word_rek::WordUpdate(){

	//orxLOG("Change word.");
	
	//If there are 10 words, choose an existing one in flight. Otherwise get a new one.
	if (this->GetCountFromCache() == 10 ){
		int i = orxMath_GetRandomU32(0, 9);
		ScrollObject *scrollObject = GetObject(this->GetGuidAtIndex(i));
		orxOBJECT *object = scrollObject->GetOrxObject();
		
		const orxSTRING word = orxObject_GetTextString(object);
		orxObject_SetTextString(wordToTarget, word); 
		//orxLOG("Got a word from index object %d and the word is: %s", i, word);
	} else {
		if (orxConfig_PushSection("ReadingList") == orxTRUE){
			const orxSTRING word = orxConfig_GetListString("Words", -1);
			
			//orxLOG("Random word from config is: %s", word);
			orxObject_SetTextString(wordToTarget, word); 
			
		}
	}

	//orxLOG("Change word complete.");
	
	PlaySound(GetTargetWord());
	
	UpdateFLashingOnAllWords();
	BoostUpLowestWords(); 
}

void word_rek::PlaySound(const orxSTRING wordToPlay)
{
	orxOBJECT *cannonObject = cannon->GetOrxObject();
	if (cannonObject == orxNULL){
		return;
	}
	
	if (orxObject_GetLastAddedSound(cannonObject) != orxNULL) {
		orxObject_RemoveSound(cannonObject, wordToPlay);
	}
	orxOBJECT *sceneObject = scene->GetOrxObject();
	if (sceneObject != orxNULL){
		scene->AddSound(wordToPlay);
	}
}

void word_rek::IncreaseWordSuccess()
{
	countWordsSuccessful++;
	wordsShotPerRound++;
	if (countWordsSuccessful > 0 && countWordsSuccessful % 5 == 0){
		Encourage();
	}

	orxCHAR formattedScore[10] = {};
	orxString_NPrint(formattedScore, sizeof(formattedScore) - 1, "%d", countWordsSuccessful);
	
	orxObject_SetTextString(hitsUI, formattedScore); 
}

void word_rek::Encourage()
{
	orxObject_CreateFromConfig("Encourage");
}

void word_rek::UpdateFLashingOnAllWords(){

	int totalWordsInPlay = GetCountFromCache();

	int i;
	for (i=0; i<totalWordsInPlay; i++){
		orxU64 iGuid = GetGuidAtIndex(i);
		ScrollObject *wordObject = GetObject(iGuid);
		TryFlashOnWord(wordObject);
	}
}

void word_rek::BoostUpLowestWords(){

	int totalWordsInPlay = GetCountFromCache();
	
	int i;
	for (i=0; i<totalWordsInPlay; i++){
		orxVECTOR speedUp = {orxMath_GetRandomFloat(-300.0, 300.0), orxMath_GetRandomFloat(-300.0, 300.0), 0};
		orxU64 iGuid = GetGuidAtIndex(i);
		ScrollObject *wordObject = GetObject(iGuid);
		
		orxVECTOR position = {0, 0, 0};
		wordObject->GetPosition(position, orxFALSE);
		
		wordObject->SetSpeed(speedUp, orxFALSE);	
		
	}
}

void word_rek::TryFlashOnWord(ScrollObject *wordObject){
	
	const orxSTRING currentTargetWord = GetTargetWord();
	orxVECTOR whiteVector = {1.0, 1.0, 1.0};
	orxCOLOR whiteColour;
	whiteColour.vRGB = whiteVector;
	whiteColour.fAlpha = orxFLOAT_1;
	
	if (wordObject){
		orxOBJECT *object = wordObject->GetOrxObject();
		const orxSTRING wordText = orxObject_GetTextString(object);
		wordObject->RemoveFX("OrangeColourCycle");
		
		if (orxString_Compare(wordText, currentTargetWord) == 0){
			//float f = orxMath_GetRandomFloat(0.0, 0.5);
			//orxVECTOR flashLevel = { 1.0, 0.5f + f, f*2};
			//orxLOG("x: %f y: %f z: %f", flashLevel.fX, flashLevel.fY, flashLevel.fZ );
			if (orxConfig_PushSection("OrangeColourCycle")){
				orxConfig_SetVector("StartValue", &flashLevel);
				orxConfig_PopSection();
			}
			wordObject->AddFX("OrangeColourCycle");
		} else {
			wordObject->SetColor(whiteColour, orxFALSE);
		}
	}
}

const orxSTRING word_rek::GetTargetWord(){
	return orxObject_GetTextString(wordToTarget);
}

/** Init function, it is called when all orx's modules have been initialized
 */
orxSTATUS word_rek::Init()
{
	orxMath_InitRandom((orxS32)orxSystem_GetRealTime()); //a good seeding value

	titleScreen = CreateObject("TitleScreen");
	scope = CreateObject("Scope");
		
	//HideTitleScreen();

	//StartNewGame();
		
    // Done!
    return orxSTATUS_SUCCESS;
}

/** Run function, it should not contain any game logic
 */
orxSTATUS word_rek::Run()
{
    // Return orxSTATUS_FAILURE to instruct orx to quit
    return orxSTATUS_SUCCESS;
}

/** Exit function, it is called before exiting from orx
 */
void word_rek::Exit()
{
    // Let Orx clean all our mess automatically. :)
}

/** BindObjects function, ScrollObject-derived classes are bound to config sections here
 */
void word_rek::BindObjects()
{
    // Bind the Object class to the Object config section
    ScrollBindObject<Scope>("Scope");
	ScrollBindObject<Cannon>("Cannon");
	ScrollBindObject<Word>("Word");

	ScrollBindObject<TitleScreen>("TitleScreen");
	ScrollBindObject<GameOver>("GameOver");
}

/** Bootstrap function, it is called before config is initialized, allowing for early resource storage definitions
 */
orxSTATUS word_rek::Bootstrap() const
{
    // Add config storage to find the initial config file
    orxResource_AddStorage(orxCONFIG_KZ_RESOURCE_GROUP, "../data/config", orxFALSE);

    // Return orxSTATUS_FAILURE to prevent orx from loading the default config file
    return orxSTATUS_SUCCESS;
}

void word_rek::AddWordGuidToCache(orxU64 guid){
	wordCache.push_back(guid);
}
void word_rek::RemoveWordGuidFromCache(orxU64 guid){
	int i, at;
	for (i=0; i<wordCache.size(); i++){
		if (wordCache.at(i) == guid){
			//orxLOG("Removing at %d, value %lld guid %lld", i, wordCache.at(i), guid);
			wordCache.erase(wordCache.begin() + i);
			return;
		}
	}
}
int	word_rek::GetCountFromCache(){
	return wordCache.size();
}
orxU64 word_rek::GetGuidAtIndex(int index){
	return wordCache.at(index);
}

/** Main function
 */
int main(int argc, char **argv)
{
    // Execute our game
    word_rek::GetInstance().Execute(argc, argv);

    // Done!
    return EXIT_SUCCESS;
}

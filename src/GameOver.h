
#ifndef __GAMEOVER_H__
#define __GAMEOVER_H__

#include "word-rek.h"

/** Object Class
 */
class GameOver : public ScrollObject
{
public:


protected:

                void            OnCreate();
                void            OnDelete();
                void            Update(const orxCLOCK_INFO &_rstInfo);


private:

};

#endif // __GAMEOVER_H__
